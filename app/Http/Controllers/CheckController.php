<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CheckController extends Controller
{
    public function index()
    {
    	// mengirim data pegawai ke view index
         return view('check-in');
    }

    public function cari(Request $request)
	{
		// menangkap data pencarian
		$cari = $request->cari;
 
    		// mengambil data dari table pegawai sesuai pencarian data
		$tiket = DB::table('tiket')
		->where('kode','like',"%".$cari."%")
		->paginate();
 
    		// mengirim data pegawai ke view index
		return view('pemesanan',['tiket' => $tiket]);
 
	}

}
