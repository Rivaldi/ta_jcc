<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function index()
    {
         $tiket = DB::table('tiket')->get();
    	// mengirim data pegawai ke view index
    	return view('pemesanan',['tiket' => $tiket]);
    }
    // method untuk insert data ke table pegawai
public function tambah(Request $request)
{
	// insert data ke table pegawai
	 DB::table('tiket')->insert([
	 	'kode' => $request->kode,
		'nama' => $request->nama,
		'umur' => $request->umur,
		'alamat' => $request->alamat,
		'email' => $request->email,
		'status' => $request->status
	]);
	 $tiket = DB::table('tiket')->where('kode',$request->kode)->get();

	// alihkan halaman ke halaman pegawai
	return view('berhasil',['tiket' => $tiket]);

}

	public function edit($id)
	{
		$tiket = DB::table('tiket')->where('kode',$id)->get();
		return view('edit',['tiket' => $tiket]);
	 
	}

	public function cetak($id)
	{
		$tiket = DB::table('tiket')->where('kode',$id)->get();
		return view('cetak',['tiket' => $tiket]);
	 
	}

	public function update(Request $request)
	{
	// update data Pemesanan
	DB::table('tiket')->where('kode',$request->kode)->update([
		'nama' => $request->nama,
		'umur' => $request->umur,
		'alamat' => $request->alamat,
		'email' => $request->email,
		'status' => $request->status
	]);
	// alihkan halaman ke halaman pemesanan
	$tiket = DB::table('tiket')->get();
    	// mengirim data pegawai ke view index
    	return view('pemesanan',['tiket' => $tiket]);
	}


	// method untuk hapus data pegawai
public function hapus($id)
{
	// menghapus data pegawai berdasarkan id yang dipilih
	DB::table('tiket')->where('kode',$id)->delete();
		
	// alihkan halaman ke halaman pegawai
	return redirect('/pemesanan');
}


}
