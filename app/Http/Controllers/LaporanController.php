<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LaporanController extends Controller
{
    public function index()
    {
    	$tiket = DB::table('tiket')->get();
    	// mengirim data pegawai ke view index
        return view('laporan',['tiket' => $tiket]);
    }

    public function cari(Request $request)
	{
		// menangkap data pencarian
		$cari = $request->cari;
 
    		// mengambil data dari table pegawai sesuai pencarian data
		$tiket = DB::table('tiket')
		->where('status','like',"%".$cari."%")
		->paginate();
 
    		// mengirim data pegawai ke view index
		return view('laporan',['tiket' => $tiket]);
 
	}

}
