<?php
$koneksi = mysqli_connect("localhost","root","","pemesanan_tiket");
$query = mysqli_query($koneksi, "SELECT max(kode) as kodeTerbesar FROM tiket");
$data = mysqli_fetch_array($query);
$kodea = $data['kodeTerbesar'];
$urutan = (int) substr($kodea, 3, 3);
$urutan++; 
$huruf = "";
$tanggal = date('ymdhis');
$kode = sprintf("%03s", $urutan);
$kode_urut = $kodea+1;
 
?><!--
=========================================================
* Argon Dashboard - v1.2.0
=========================================================
* Product Page: https://www.creative-tim.com/product/argon-dashboard


* Copyright  Creative Tim (http://www.creative-tim.com)
* Coded by www.creative-tim.com



=========================================================
* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-->
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>Pemesanan Tiket</title>
  <!-- Favicon -->
  <link rel="icon" href="{{url('argon-dashboard/assets/img/brand/favicon.png')}}" type="image/png">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <!-- Icons -->
  <link rel="stylesheet" href="{{url('argon-dashboard/assets/vendor/nucleo/css/nucleo.css')}}" type="text/css">
  <link rel="stylesheet" href="{{url('argon-dashboard/assets/vendor/@fortawesome/fontawesome-free/css/all.min.css')}}" type="text/css">
  <!-- Page plugins -->
  <!-- Argon CSS -->
  <link rel="stylesheet" href="{{url('argon-dashboard/assets/css/argon.css?v=1.2.0')}}" type="text/css">
</head>

<body>
  @include('layouts.header_form')

  <div class="container-fluid mt--6">

<div class="row">
        <div class="col-xl-12">
          <div class="card">
            <div class="card-header bg-transparent">
              <div class="row align-items-center">
                <div class="col">
                  <h5 class="h3 text-default mb-0">PEMESANAN TIKET</h5>
                  <h6 class="text-default text-uppercase ls-1 mb-1">Tiket Konser BogorFest</h6>
                </div>
              </div>
              <br>
              <div>
                    
                  <form action="/pemesanan/tambah/" method="post" role="form">
                    {{ csrf_field() }}
                    <div class="row">
                        <input type="hidden" id="input-username" class="form-control" placeholder="Username" value="<?php echo $kode_urut; ?>" readonly="" name="kode">
                     
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-email">Nama Pemesan</label>
                        <input name="nama" type="text" class="form-control">
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-email">Umur Pemesan</label>
                        <input name="umur" type="number" class="form-control">
                      </div>
                    </div>
                    <div class="col-lg-12">
                      <div class="form-group">
                        <label class="form-control-label" for="input-username">Email</label>
                        <input type="email" id="input-username" class="form-control" placeholder="" value="" name="email">
                      </div>
                    </div>
                    <div class="col-lg-12">
                      <div class="form-group">
                        <label class="form-control-label" for="input-username">Alamat</label>
                        <textarea name="alamat" class="form-control"></textarea>
                      </div>
                    </div>
                    <div class="col-lg-12">
                      <div class="form-group">
                        <input type="hidden" value="0" class="form-control" name="status" readonly>
                      </div>
                    </div>
                    <div class="col-lg-12">
                      <div class="form-group">
                        <div class="text-center">
                          <input type="submit" class="btn btn-primary mt-12" value="Simpan">
                        </div>
                      </div>
                    </div>
                  </div>
                  </form>
              </div>
            </div>
          </div>
        </div>
</div>
  @include('layouts.footer')
  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="{{url('argon-dashboard/assets/vendor/jquery/dist/jquery.min.js')}}"></script>
  <script src="{{url('argon-dashboard/assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{url('argon-dashboard/assets/vendor/js-cookie/js.cookie.js')}}"></script>
  <script src="{{url('argon-dashboard/assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js')}}"></script>
  <script src="{{url('argon-dashboard/assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js')}}"></script>
  <!-- Optional JS -->
  <script src="{{url('argon-dashboard/assets/vendor/chart.js')}}/dist/Chart.min.js')}}"></script>
  <script src="{{url('argon-dashboard/assets/vendor/chart.js')}}/dist/Chart.extension.js')}}"></script>
  <!-- Argon JS -->
  <script src="{{url('argon-dashboard/assets/js/argon.js')}}?v=1.2.0')}}"></script>
</body>

</html>
