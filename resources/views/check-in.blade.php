@extends('layouts.app')


@section('content')

<div class="row">
        <div class="col-xl-12">
          <div class="card">
            <div class="card-header bg-transparent">
              <div class="row align-items-center">
                <div class="col">
                  <h5 class="h3 text-default mb-0">CHECK-IN</h5>
                  <h6 class="text-default text-uppercase ls-1 mb-1">Check-In Tiket Konser BogorFest</h6>
                </div>
              </div>
              <br>
              <div>
                <div class="table-responsive">
                  <form action="/check/cari" method="GET" role="form">
                    <div class="form-group">
                      <div class="input-group input-group-merge input-group-alternative mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text"><i class="ni ni-key-25"></i></span>
                        </div>
                        <input name="cari" class="form-control" placeholder="Kode Booking Tiket" type="text">
                      </div>
                    </div>
                    <div class="text-center">
                      <input type="submit" class="btn btn-primary mt-4" value="Check-In Tiket" >
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
@endsection