<nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
      <!-- Brand -->
      <div class="sidenav-header  align-items-center">
        <a class="navbar-brand" href="javascript:void(0)">
          <img src="{{url('argon-dashboard/assets/img/brand/musik.png')}}" width="85%" height="100%" class="navbar-brand-img" alt="..."><br>
          <p>Pemesanan Tiket Konser</p>
        </a>
      </div>
      <div class="navbar-inner">
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
          <!-- Nav items -->
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link active" href="{{url('home')}}">
                <i class="ni ni-tv-2 text-primary"></i>
                <span class="nav-link-text">Dashboard</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{url('pemesanan')}}">
                <i class="ni ni-single-02 text-orange"></i>
                <span class="nav-link-text">Pemesanan</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{url('check-in')}}">
                <i class="ni ni-pin-3 text-primary"></i>
                <span class="nav-link-text">Check-In</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{url('laporan')}}">
                <i class="ni ni-books text-yellow"></i>
                <span class="nav-link-text">Laporan</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </nav>