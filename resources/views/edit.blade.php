@extends('layouts.app')


@section('content')
@foreach($tiket as $data)
<div class="row">
        <div class="col-xl-12">
          <div class="card">
            <div class="card-header bg-transparent">
              <div class="row align-items-center">
                <div class="col">
                  <h5 class="h3 text-default mb-0">Perubahan Data #{{ $data->kode }}</h5>
                  <h6 class="text-default text-uppercase ls-1 mb-1">Ubah data Tiket Konser BogorFest</h6>
                </div>
              </div>
              <br>
              <div>
                    
                  <form action="/pemesanan/update/" method="post" role="form">
                    {{ csrf_field() }}
                    <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-username">Kode Tiket</label>
                        <input type="text" id="input-username" class="form-control" placeholder="Username" value="{{ $data->kode }}" readonly="" name="kode">
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-email">Nama Pemesan</label>
                        <input name="nama" type="text" class="form-control" value="{{ $data->nama }}" placeholder="">
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-email">Umur Pemesan</label>
                        <input value="{{ $data->umur }}" name="umur" type="number" class="form-control">
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-username">Email</label>
                        <input value="{{ $data->email }}" type="email" id="input-username" class="form-control" placeholder="" value="" name="email">
                      </div>
                    </div>
                    <div class="col-lg-12">
                      <div class="form-group">
                        <label class="form-control-label" for="input-username">Alamat</label>
                        <textarea name="alamat" class="form-control">{{ $data->alamat }}</textarea>
                      </div>
                    </div>
                    <div class="col-lg-12">
                      <div class="form-group">
                        <label class="form-control-label" for="input-email">Status Pemesanan</label>
                        <select class="form-control" name="status">
                          <option selected value="{{ $data->status }}">
                            <?php
                              if ($data->status == 0) {
                                echo "Belum Check-In";
                              }else{
                                echo "Sudah Check-In";
                              }
                            ?>
                          </option>
                          <?php
                            if ($data->status == 0) {
                                echo "<option value='1'>Sudah Check-In</option>";
                              }else{
                                echo "<option value='0'>Belum Check-In</option>";
                              }
                          ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-lg-12">
                      <div class="form-group">
                        <div class="text-center">
                          <input type="submit" class="btn btn-primary mt-12" value="Simpan">
                        </div>
                      </div>
                    </div>
                  </div>
                  </form>
                  @endforeach
              </div>
            </div>
          </div>
        </div>
@endsection
