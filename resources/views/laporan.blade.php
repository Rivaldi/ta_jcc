@extends('layouts.app')


@section('content')

<form action="/laporan/cari" method="GET" role="form">
  <div class="card">
    <div class="container">
<div class="row">
  <div class="col-lg-9">
    <div class="form-group"><br>
      <h5 class="h3 text-default mb-0">STATUS TIKET</h5>
      <h6></h6>
      <select name="cari" class="form-control" placeholder="Kode Booking Tiket">
        <option value="">Semua</option>
        <option value="1">Sudah Check-In</option>
        <option value="0">Belum Check-In</option>
      </select>
    </div>
  </div>
  <div class="col-lg-3">
    <div class="form-group">
      <input type="submit" class="btn btn-primary" style="margin-top: 57px " value="Check-In Tiket" >
    </div>
  </div>
</div></div>
</div>
</form>
<div class="row">
        <div class="col-xl-12">
          <div class="card">
            <div class="card-header bg-transparent">
              <div class="row align-items-center">
                <div class="col">
                  <h5 class="h3 text-default mb-0">LAPORAN PEMESANAN</h5>
                  <h6 class="text-default text-uppercase ls-1 mb-1">Tiket Konser BogorFest</h6>
                </div>
              </div>
              <br>
              <div>
                <div class="table-responsive">
                  <table class="table align-items-center table-flush">
                    <thead class="thead-light">
                      <tr>
                        <th scope="col" class="sort" data-sort="budget">Kode Tiket</th>
                        <th scope="col" class="sort" data-sort="status">Nama</th>
                        <th scope="col">Status</th>
                      </tr>
                    </thead>
                    <tbody class="list">
                      @foreach($tiket as $data)
                        <tr>
                          <td>{{ $data->kode }}</td>
                          <td>{{ $data->nama }}</td>
                          <td><?php
                              if ($data->status == 0) {
                                echo "Belum Check-In";
                              }else{
                                echo "Sudah Check-In";
                              }
                            ?></td>
                        </tr>
                        @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
@endsection