<!--
=========================================================
* Argon Dashboard - v1.2.0
=========================================================
* Product Page: https://www.creative-tim.com/product/argon-dashboard


* Copyright  Creative Tim (http://www.creative-tim.com)
* Coded by www.creative-tim.com



=========================================================
* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-->
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>Pemesanan Tiket</title>
  <!-- Favicon -->
  <link rel="icon" href="{{url('argon-dashboard/assets/img/brand/favicon.png')}}" type="image/png">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <!-- Icons -->
  <link rel="stylesheet" href="{{url('argon-dashboard/assets/vendor/nucleo/css/nucleo.css')}}" type="text/css">
  <link rel="stylesheet" href="{{url('argon-dashboard/assets/vendor/@fortawesome/fontawesome-free/css/all.min.css')}}" type="text/css">
  <!-- Page plugins -->
  <!-- Argon CSS -->
  <link rel="stylesheet" href="{{url('argon-dashboard/assets/css/argon.css?v=1.2.0')}}" type="text/css">
</head>

<body>
  @include('layouts.header_form')
  <div class="container-fluid mt--6">
    <div class="row" style="margin-left:9%">
        <div class="col-xl-11" align="center">
          <div class="card" style="padding: 35px">
            <div style="float: left;">
              <a href="javascript:printDiv('print');"  class="btn btn-primary mt-3">Print</a>
            </div>
            <div class="card-header bg-transparent" id="print">
              <div class="row align-items-center">
                <div class="col">
                  <h5 class="h3 text-default mb-0">PEMESANAN TIKET</h5>
                  <h6 class="text-default text-uppercase ls-1 mb-1">Tiket Konser BogorFest</h6>
                </div>
              </div>
              <br>
              <div>
                @foreach($tiket as $data)
                  <h1 class="h1 text-default mb-0">Selamat!</h1>
                  <h5 class="h3 text-default mb-0">Selamat {{ $data->nama }} pemesanan tiket Anda telah berhasil.<br>Anda telah terdaftar di
                  acara Konser BogorFest. Saat ini status tiket Anda
                  adalah Approved.</h5><br>
                    <table align="left">
                    <tr>
                      <td>Tanggal</td>
                      <td>:</td>
                      <td>27 September 2021</td>
                    </tr>
                    <tr>
                      <td>Waktu</td>
                      <td>:</td>
                      <td>19:00 WIB - 21:00 WIB</td>
                    </tr>
                    <tr>
                      <td>Tempat</td>
                      <td>:</td>
                      <td>Stadion Pakansari Cibinong</td>
                    </tr>
                  </table>
                  <div style="margin-top: 95px"></div>
                  <h3 class="h3 text-default mb-0 text-left">Syarat dan Ketentuan</h3>
                  <h6 class="text-default text-uppercase ls-1 mb-1 text-left">1. Anda dapat menonton live streaming acara ini dengan cara mengakses link
                  yang ada di bagian e-tiket.<br>
                  2. Link streaming yang ada di e-ticket Anda bersifat pribadi. Tidak diperkenankan
                  diberikan kepada siapapun.</h6><br>
                  <img src="{{url('argon-dashboard/assets/img/brand/musik.png')}}" width="15%" height="100%" class="navbar-brand-img" alt="..."><br>
                  <h6 class="text-default text-uppercase ls-1 mb-1">Phone : 021-7918 7722 or Email : info@konser.com<br>
                  Copyright @ 2021 BogorFest. All right reserved</h6>
                @endforeach
              </div>
            </div>
          </div>
        </div>
</div>
  @include('layouts.footer')
  <textarea id="printing-css" style="display:none;">.no-print{display:none}</textarea>
<iframe id="printing-frame" name="print_frame" src="about:blank" style="display:none;"></iframe>
<script type="text/javascript">
function printDiv(elementId) {
 var a = document.getElementById('printing-css').value;
 var b = document.getElementById(elementId).innerHTML;
 window.frames["print_frame"].document.title = document.title;
 window.frames["print_frame"].document.body.innerHTML = '<style>' + a + '</style>' + b;
 window.frames["print_frame"].window.focus();
 window.frames["print_frame"].window.print();
}
</script>
  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="{{url('argon-dashboard/assets/vendor/jquery/dist/jquery.min.js')}}"></script>
  <script src="{{url('argon-dashboard/assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{url('argon-dashboard/assets/vendor/js-cookie/js.cookie.js')}}"></script>
  <script src="{{url('argon-dashboard/assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js')}}"></script>
  <script src="{{url('argon-dashboard/assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js')}}"></script>
  <!-- Optional JS -->
  <script src="{{url('argon-dashboard/assets/vendor/chart.js')}}/dist/Chart.min.js')}}"></script>
  <script src="{{url('argon-dashboard/assets/vendor/chart.js')}}/dist/Chart.extension.js')}}"></script>
  <!-- Argon JS -->
  <script src="{{url('argon-dashboard/assets/js/argon.js')}}?v=1.2.0')}}"></script>
</body>

</html>
