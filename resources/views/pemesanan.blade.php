@extends('layouts.app')


@section('content')

<div class="row">
        <div class="col-xl-12">
          <div class="card">
            <div class="card-header bg-transparent">
              <div class="row align-items-center">
                <div class="col">
                  <h5 class="h3 text-default mb-0">PEMESANAN</h5>
                  <h6 class="text-default text-uppercase ls-1 mb-1">Tiket Konser BogorFest</h6>
                </div>
              </div>
              <br>
              <div>
                <div class="table-responsive">
                  <table class="table align-items-center table-flush">
                    <thead class="thead-light">
                      <tr>
                        <th scope="col" class="sort" data-sort="budget">Kode Tiket</th>
                        <th scope="col" class="sort" data-sort="status">Nama</th>
                        <th scope="col">Email</th>
                        <th scope="col">Umur</th>
                        <th scope="col">Alamat</th>
                        <th scope="col">Aksi</th>
                      </tr>
                    </thead>
                    <tbody class="list">
                      @foreach($tiket as $data)
                        <tr>
                          <td>{{ $data->kode }}</td>
                          <td>{{ $data->nama }}</td>
                          <td>{{ $data->email }}</td>
                          <td>{{ $data->umur }}</td>
                          <td>{{ $data->alamat }}</td>
                          <td>
                            <?php
                              if ($data->status == 0) {?>
                                
                                  <a class="btn btn-primary" href="/pemesanan/edit/{{ $data->kode }}">Edit</a>
                                  <a class="btn btn-warning" href="/pemesanan/hapus/{{ $data->kode }}">Hapus</a>
                              <?php
                              }else{
                                echo "Sudah Check-In";
                              }
                            ?>
                            
                          </td>
                        </tr>
                        @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
@endsection