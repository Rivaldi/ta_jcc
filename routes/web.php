<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('form_tiket');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/form_tiket', function () {
    return view('form_tiket');
});
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::get('/pemesanan', 'DashboardController@index');
Route::post('/pemesanan/tambah', 'DashboardController@tambah');
Route::get('/pemesanan/edit/{id}','DashboardController@edit');
Route::post('/pemesanan/update','DashboardController@update');
Route::get('/pemesanan/hapus/{id}','DashboardController@hapus');
Route::get('/pemesanan/cetak/{id}','DashboardController@cetak');



Route::get('/check-in', 'CheckController@index');
Route::get('/check/cari','CheckController@cari');


Route::get('/laporan', 'LaporanController@index');
Route::get('/laporan/cari','LaporanController@cari');
